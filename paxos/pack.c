
#define PutDWord(p, offset, value) *((uint32_t *)(p + offset)) = htonl(value)

#define PutWord(p, offset, value) *((uint16_t *)(p + offset)) = htons(value)

void pack_value(char *p, paxos_value *v, int offset)
{
    pack_uint32(p, v->paxos_value_len, offset);
    char *value =  (char *)(p + offset + 4);
    memcpy(value, v->paxos_value_val, v->paxos_value_len);
}
