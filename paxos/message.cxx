#include "message.hpp"

bool operator!=(MCmdMsg& a, MCmdMsg& b){
	if (a.size != b.size) return false;
	else if (a.ptr == b.ptr) return true;
	else return memcmp(a.ptr, b.ptr, a.size);
}