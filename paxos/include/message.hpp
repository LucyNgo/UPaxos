#ifndef MESSAGE_H
#define MESSAGE_H

#include <stdint.h>
#include <arpa/inet.h>

#define Word   uint16_t
#define DWord  uint32_t
#define Bytes  char*

#if __cplusplus
extern "C" {
#endif

	enum PaxosType {
		PaxosPrepare,
		PaxosPromise,
		PaxosAccept,
		PaxosAccepted,
		PaxosPreempted,
		PaxosRepeat,
		PaxosTrim,
		PaxosAcceptState,
		PaxosClientValue
	};

	typedef struct MCmdMsg {
		DWord  size;
		Bytes  ptr;
	} MCmdMsg;

	typedef struct MLearnerMsg {
		DWord   iid;
		Word    ballot, thid;
		Word    atid, aid, bvalue;
		MCmdMsg cmd;
	} MLearnerMsg;

	typedef struct ARepeatMsg {
		DWord from, to;
		Word  thid, atid;
	} ARepeatMsg;

#define TrimMsg DWord

	typedef struct AActStateMsg {
		Word triid, atid;
	} AActStateMsg;

#define TRIM_OFFSET         2

#define getDWord(p, offset) ntohl(*((uint32_t*)((char*)p + offset)))
#define getWord(p, offset)  ntohs(*((uint16_t*)((char*)p + offset)))
#define getValue(p, offset) (char*)((char*)p + offset + 4)
#define getObject(p, offset, object) *((object*)((char*)p+ offset))

#define getTrimMsg(p) getDWord(p, TRIM_OFFSET)
#define getType(p) getWord(p, 0)

	MLearnerMsg getPrepareMsg(const char* p, int size);
	MLearnerMsg getPromiseMsg(const char* p, int size);
	MLearnerMsg getAcceptMsg(const char* p, int size);
	MLearnerMsg getAcceptedMsg(const char* p, int size);
	MLearnerMsg getPreemptMsg(const char* p, int size);
	ARepeatMsg getRepeatMsg(const char* p, int size);
	AActStateMsg getAcceptorStateMsg(const char* p, int size);
	MCmdMsg getClientCmdMsg(const char* p, int size);
	int getThreadId(const char* p);

#if __cplusplus
}

#include <cstring>

bool operator!= (MCmdMsg& a, MCmdMsg& b);
#endif
#endif