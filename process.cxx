#include "playground.hpp"
#include "message.hpp"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

/* mainboard, nơi thiết kế cách hoạt động chính của chương trình */

/* hàm này có trách nhiệm đọc các token từ 1 file stream (fstream) lên và nạp vào vector
   số token cần đọc được quy định bằng count
 */

typedef struct sockaddr_in sockaddr_in;

struct ClientReq {
	sockaddr_in address;
	char* 		ptr;
	size_t 		size;

	ClientReq(MCmdMsg& cmd): ptr{nullptr} {
		if (cmd.ptr) {
			size    = getWord(cmd.ptr, 0) - sizeof(Word) - sizeof(sockaddr_in) - 1;
			address = getObject(cmd.ptr, sizeof(Word), sockaddr_in);
			ptr 	= getValue(cmd.ptr, sizeof(Word) + sizeof(sockaddr_in) + 1);
		}
	}
};

struct Command {
	enum Operator { Get, Set };

	timespec ts;
	int 	 id;
	Operator op;
	char 	 dt[32];

	Command(ClientReq& request) {
		if (request.ptr) {
			ts = getObject(request.ptr, 0, timespec);
			id = getWord(request.ptr, sizeof(timespec));
			op = getObject(request.ptr, sizeof(timespec) + sizeof(Word), Operator);

			memcpy(dt, request.ptr + sizeof(timespec) + sizeof(Word) + sizeof(Operator), 32);
		}
	}
};

void deliver(int sockid, MCmdMsg &msg, TLearner& learner) {
	ClientReq request{msg};

	if (config.content().leveldb) {
		Command cmd{request};

		switch (cmd.op) {
		case Command::Set:
			if (add_entry(learner.leveldb, 0, cmd.dt, 16, &cmd.dt[16], 16))
				fprintf(stderr, "Add entry failed.\n");
			break;

		case Command::Get: {
			char* stored_value = nullptr;
			size_t vsize = 0;

			if (get_value(learner.leveldb, cmd.dt, 16, &stored_value, &vsize))
				fprintf(stderr, "get value failed.\n");
			else if (stored_value) {
				printf("Stored value %s, size %zu", stored_value, vsize);
				free(stored_value);
			}
			break;
		}
		}

		if (config.iddentity == 0) {
			int n = sendto(sockid, request.ptr, request.size, 0,
			               (struct sockaddr*)&request.address,
			               sizeof(request.address));
			if (n < 0) perror("deliver: sendto error");
		}
	}
}

bool load(std::fstream & cfg, std::vector<std::string>& output, std::size_t count) {
	std::string value;

	for (; count > 0 && !cfg.eof(); --count) {
		cfg >> value;
		output.push_back(value);
	}
	return count == 0;
}

template<typename T>
void send(int sockid, PaxosType type, T msg, std::vector<Caans::Address>& acceptors) {
	char 		header[128];
	sockaddr_in addr;

	for (auto& acceptor : acceptors) {
		addr.sin_family = AF_INET;
		addr.sin_addr.s_addr = acceptor.address();
		addr.sin_port = htons(acceptor.port());

		memcpy(header, &type, sizeof(Word));
		memcpy(header + sizeof(Word), &msg, sizeof(T));
		sendto(sockid, header,
		       sizeof(Word) + sizeof(T), 0,
		       (struct sockaddr*)&addr, sizeof(sockaddr_in));
	}
}

int deserilization(char* ptr, int size, Protocol & msg) {
	ptr[size] = '\0';
	return getThreadId(msg = ptr);
};

/* check holes sau một chu kì hoặc khi số lượng wait cao hơn một mức xác định */
void vanquish(int sockid, std::size_t thread, Process & content) {
	auto to   = content.machine.hiwait();
	auto from = content.machine.running();

	if (to > from) {
		for (auto iid = from; iid < to; ++iid) {
			send(sockid, PaxosPrepare,
			     MLearnerMsg{DWord(iid), 3, Word(thread)},
			     config.content().acceptors);
		}
	}
}

/* hàm này chịu trách nhiệm phân tích và xử lý data ngay tại chỗ luôn */
upaxos::Error handle(int sockid, Executor * exec, char* msg, int size) {
	try {
		if (Cast(exec)->content().machine.track() > config.content().track)
			vanquish(sockid, exec->id, Cast(exec)->content());
		if (msg && size > 0) {
			switch (getType(msg)) {
			case PaxosPrepare:
				break;

			case PaxosPromise:
				break;

			case PaxosAccept: {
				MLearnerMsg accept = getAcceptMsg(msg, size);
				MCmdMsg 	cmd;
				paxos::IId 	iid;

				Cast(exec)->content().machine.load(std::make_tuple(accept.iid, accept.aid),
				                                   accept.ballot,
				                                   accept.cmd);

				while ((iid = Cast(exec)->content().machine.deliver(cmd)) != std::string::npos)
					deliver(sockid, cmd, Cast(exec)->content());
				break;
			}

			default:
				break;
			}
		}
		return NoError;
	} catch (upaxos::Error& error) { return error;}
}