#include "system.hpp"

namespace upaxos {
Error::Error(std::string debug, int code, std::string message):
	message(message), debug(debug), code{code} {
#if DEBUG
	if (code == 0) return;
	std::cout << "[" << codename() << "]: "
	          << message << " at "
	          << debug << std::endl;
#endif
}

Error::~Error() {}

Error::operator bool() { return code != 0; }

Error::operator int() { return code; }

std::string Error::codename() { return std::to_string(code); }

std::string Error::position(const char* file, int line, const char* proc) {
	std::string result;

	result.append(file).append(":")
		  .append(std::to_string(line))
		  .append(" ")
		  .append(proc, strlen(proc));
	return result;
}
}
std::ostream& operator <<(std::ostream& stream, upaxos::Error error) {
	stream << "[" << error.codename() << "]: "
	       << error.message << " at "
	       << error.debug << std::endl;
	return stream;
}