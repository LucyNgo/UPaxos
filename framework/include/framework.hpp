#if !defined(VARIABLE_H) && __cplusplus
#define VARIABLE_H

#include "system.hpp"
#include "udpev.h"

#include <functional>
#include <fstream>
#include <string>
#include <vector>

struct Controller;

namespace upaxos {
/* - Mỗi một thread là 1 đại diện cho 1 biến ảo tính toán
   - Ví dụ biến X, biến Y, biến Z, vậy Variable có vai trò như biến ảo,
   nhận các tính toán trên biến ảo. Mỗi thread đại diện cho 1 biến ảo
   và vì thế ta có thể hiểu đơn giản Variable chính là 1 thread
   thực thi (executor)
 */

struct Variable {
protected:
	Variable();

public:
	virtual ~Variable();
};

/* Configure's implementation */
template<typename Content, typename Process>
struct Configure {
	using Token       = std::function<std::size_t(std::fstream&, std::vector<std::string>&)>;
	using Transaction = std::function<upaxos::Error(Content&, std::vector<std::string>&)>;
	using Initation   = std::function<void(Content&, Variable*)>;
	using Owner       = std::function<in_addr_t(Content&)>;

	Configure(Token token, std::vector<Transaction> transactions, Initation initation, Owner owner = nullptr)
		: _transactions(transactions), _initation{initation}, _token(token), _owner(owner) {}

	std::size_t iddentity = std::string::npos;

	upaxos::Error open(const char* path) {
		std::vector<std::string> tokens;
		std::fstream file{path};
		bool opened{file.is_open()};

		while (!file.eof() && opened) {
			auto index = _token(file, tokens);

			if (index != std::string::npos && index < _transactions.size() && _transactions[index]) {
				upaxos::Error error = _transactions[index](_content, tokens);
				if (error) return error;
			}
		}

		return opened? NoError: NotFound;
	}

	void install(Variable* var) { if (_initation) _initation(_content, var); }

	Content& content() { return _content; }

	in_addr_t owner() { return _owner(_content); }

private:
	std::vector<Transaction> _transactions;
	Content 				 _content;
	Initation 				 _initation;
	Token 					 _token;
	Owner 					 _owner;
};

/* các receiver cần có 1 bộ giải mã(unpack) được khai báo toàn cục */
template <typename Msg>
struct ParseImpl {
	using Parser = std::function<int(char*, int, Msg&)>;

	ParseImpl(Parser parse) : parse{parse} {}

	virtual ~ParseImpl() {}

	virtual Msg convert(void* protocol) { throw NoSupport; }

	const std::type_info& type() { return typeid(Msg); }

	Parser parse;
};

namespace unpacking {
upaxos::Error parse(const char* ptr, int size, Contract& contract);
}

/* - VariableImpl là một mô tả chi tiết cho Variable, một lớp trừu tượng chỉ có vai trò đại diện
   - index chỉ định danh của variable
 */
template <typename Content, typename Process, typename Msg>
struct VariableImpl : Variable {
	using Deliver = std::function<Error (int, Executor*, Msg&, int)>;
private:
	Executor*   _executor;
	Deliver     _deliver;
	std::size_t _index;
	Process _content;
public:
	VariableImpl(Controller* controller, std::size_t index, Configure<Content, Process>& config)
		: Variable(), _executor(&controller->executors[index]), _index{index} {
		config.install(this);
	}

	~VariableImpl() {}

	VariableImpl<Content, Process, Msg>* exec(Executor* executor) {
		_executor = executor;
		return this;
	}

	void deliver(Deliver deliver) { _deliver = deliver; }

	Process& content() { return _content; }

	Error deliver(int socket, Msg msg, int size) { return _deliver(socket, _executor, msg, size); }

	operator int() { return _index; }
};

namespace variable {
template <typename Content, typename Process, typename  Message>
Variable* instance(Controller* controller, std::size_t index, Configure<Content, Process>& config, ParseImpl<Message>& parser) {
	if (index < static_cast<std::size_t>(controller->count)) {
		return new VariableImpl<Content, Process, Message>(controller, index, config);
	}
	return nullptr;
}

template <typename Content, typename Process, typename Message>
VariableImpl<Content, Process, Message>* cast(Variable * generic, Configure<Content, Process>&, Message&) {
	auto result = dynamic_cast<VariableImpl<Content, Process, Message>*>(generic);

	if (!result) throw BadLogic;
	return result;
}

template <typename Content, typename Process, typename Message>
VariableImpl<Content, Process, Message>* cast(Variable * generic, Configure<Content, Process>&, ParseImpl<Message>&) {
	auto result = dynamic_cast<VariableImpl<Content, Process, Message>*>(generic);

	if (!result) throw BadLogic;
	return result;
}

template <typename Content, typename Process, typename Message>
VariableImpl<Content, Process, Message>* cast(Variable * generic) {
	auto result = dynamic_cast<VariableImpl<Content, Process, Message>*>(generic);

	if (!result) throw BadLogic;
	return result;
}
}
}
#endif